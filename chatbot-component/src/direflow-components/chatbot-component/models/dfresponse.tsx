
export interface IText {
    text: string[]    
}
export interface IQuickReplies {
    quickReplies?: string[];
    title?: string;
}
export interface IFullFillMentMessage {
    message: string;
    platform: string;
    text?: IText;
    quickReplies?: IQuickReplies;
    sender?: string;
    
}
export interface IDFRESPONSE {
    success:boolean;
    intent: string;
    fulfillmentMessages: IFullFillMentMessage[],
    message: {
        Response: string;
    }
}