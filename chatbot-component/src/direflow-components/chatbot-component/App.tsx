import React, { FC, useState, SyntheticEvent } from 'react';
import { Styled } from 'direflow-component';
import styles from './App.css';
import sStyles from './styles/app.style.css';
import { Chat as ChatIcon, HighlightOff, Send as SendIcon } from '@material-ui/icons';
import { CSSTransition } from 'react-transition-group';
import { EndPointDetectIntent } from '../../config/config';
import { IDFRESPONSE, IFullFillMentMessage } from './models/dfresponse';
import { Messages } from './Message';
const { v4: uuidv4 } = require('uuid');
const DF_PLATFORM  = "PLATFORM_UNSPECIFIED"
interface IProps {
  componentTitle: string;
  endPointURL: string;
  sessionId: string;
}
interface IMessage {
  message: string;
  sender: string;
  type: string | any;
}

const App: FC<IProps> = (props) => {
  console.log('Redn');
  const msgs: IFullFillMentMessage[] = [];
  const [messages, setMessages ] = useState(msgs);
  const [textmessage, setTextMessage ] = useState("");
  const [chatBoxVisible, setChatBoxVisible ] = useState(false);
  
  
  const handleClick = async (e?: SyntheticEvent | null, manualMessage?: string ) => {
    e?.preventDefault();
    const userMessage =  manualMessage || textmessage
    const userInput = {
      "platform": "PLATFORM_UNSPECIFIED",
      "text": {
        "text": [
          userMessage
        ]
      },
      "sender": "user",
      "message": "text"
    };
    // setMessages(m => [...m, userInput ]);
    messages.push(userInput);
    setMessages([...messages]);
    setTextMessage('');
    callDF(userMessage);    
};

const callDF = async (userMessage: string) => {
  const options = {
    method: 'POST',
    body: JSON.stringify({
      queryInput: {
        text: {
          text: userMessage,
          languageCode: 'en-US'
        }
      },
      sessionId: props.sessionId
    }),
    headers: {
      'Content-Type': 'application/json'
    }
  }
  // console.log(JSON.parse(options.body).sessionId);
  const response  = await fetch(props.endPointURL, options);
  const responseData: IDFRESPONSE = await response.json();
 if(!responseData.fulfillmentMessages) {
   console.log('Response Failed')
   return;
 }
  responseData.fulfillmentMessages.forEach((ele: IFullFillMentMessage) => {
    if(ele.platform === DF_PLATFORM) {
      messages.push(ele);
    }
  });
  
  setMessages(messages);
 /* responseFullfillments.forEach(responseFullfillmentText => {
    const responseMessages = responseFullfillmentText?.text?.text;
    responseMessages?.forEach(ele => {
      const messageData = {
        message: ele,
        sender: 'bot',
        type: responseFullfillmentText?.message
      }
     // messages.push(messageData); 
    })
  })*/
//  setMessages(messages);
}

  return (
    <Styled styles={[styles,sStyles]}>
      
     <div className="body-class">
      <div id="chat-circle" onClick={() => { 
          setChatBoxVisible(true);
          }} className="btn btn-raised">
        <div id="chat-overlay">     
        </div>
          <ChatIcon style={{ 'fill': "#ffffff" }}/>
        </div>
      
      
        <CSSTransition 
            in={chatBoxVisible} 
            timeout={200} 
            unmountOnExit
            classNames="chatbox-anim">
        
        <div className="chat-box" id="chatbox">
          <div className="chat-box-header">
            ChatBot
            <span onClick={()=> {
              setChatBoxVisible(false)
            }} className="chat-box-toggle">
               <HighlightOff style={{ fill: "#ffffff", width: 24, height: 24 }} />
            </span>
          </div>

          <div className="chat-box-body">
            <div className="chat-box-overlay"></div>
              <div className="chat-logs">
                <Messages 
                  messages={messages}
                  handleQuickReply={async (quickReply: string) => { 
                    setTextMessage(quickReply);
                    handleClick(null, quickReply);
                    console.log(quickReply)
                  }}  
                />
              </div>

              
                <div className="chat-input">      
                  <form onSubmit={handleClick}>
                    <input type="text" value={textmessage} autoComplete={'off'} onChange={(e) => setTextMessage(e.target.value)} id="chat-input" placeholder="Send a message..."/>
                    <button type="submit" className="chat-submit" id="chat-submit">
                      <SendIcon style={{  textAlign: 'center', alignContent: 'center', justifyContent: 'center'  }} />
                    </button>
                  </form>
                </div>
            </div> 
        </div> 
        </CSSTransition>
     </div>
    </Styled>
  );
};

App.defaultProps = {
  componentTitle: 'Componentts Component',
  endPointURL: EndPointDetectIntent,
  sessionId: uuidv4()
}

export default App;
