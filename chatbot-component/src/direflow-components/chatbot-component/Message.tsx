import React, { useEffect, useRef } from "react";
import { IFullFillMentMessage, IText, IQuickReplies } from "./models/dfresponse";

export const Messages = ({ messages, handleQuickReply }: any) => {
    const messagesEndRef = useRef(null) as any;
    const scrollToBottom = () => {
      messagesEndRef?.current?.scrollIntoView({ behavior: "smooth" });
    };
    useEffect(scrollToBottom, [messages]);
  
    return (
      <div className="messagesWrapper">
          {messages.map((ele: IFullFillMentMessage, index: number) => (
              <div key={""+ index}>
                {ele.message === 'text' ? 
                  <RenderText 
                    messageText={ele.text} 
                    messageIndex={index}
                    senderType={ele.sender}
                  />: null }
                  
                  {ele.message === 'quickReplies' ? 
                    <RenderQuickReplies 
                      messageText={ele.quickReplies} 
                      messageIndex={index}
                      senderType={ele.sender}
                      handleQuickReply={handleQuickReply}
                    /> : null
                  } 
                  </div>
              
            ))}
        <div ref={messagesEndRef} />
      </div>
    );
  };
  
  
  const RenderText = ({ messageText, messageIndex, senderType }: any) => {
    const ele: IText = messageText;
     
      return <div>
        {ele.text.map((textEle, textIndex )=> {
       return (
         <div key={"" + messageIndex + textIndex} 
          id={"" + messageIndex + textIndex} 
          className={ senderType === 'user' ? "chat-msg user": "chat-msg self"}> 
          <div className="cm-msg-text">{textEle}</div> 
        </div> 
       )
      })} </div>
  }

  const RenderQuickReplies = ({ messageText, messageIndex, senderType, handleQuickReply }: any) => {
    const ele: IQuickReplies = messageText;
     
      return <div>
        {ele?.quickReplies?.map((textEle, textIndex )=> {
       return (
         <div key={"" + messageIndex + textIndex} 
          id={"" + messageIndex + textIndex}
          className={ senderType === 'user' ? "chat-msg user quickreply-content": "chat-msg self quickreply-content"}> 
              <button className="quickreply-button" onClick={() => handleQuickReply(textEle)}>
                  {textEle}
              </button>
       
        </div> 
       )
      })} </div>
  }