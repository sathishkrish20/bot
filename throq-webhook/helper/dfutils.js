

module.exports.getContext = (agent, contextName) => {
    contextName = String(contextName).toLowerCase();
    const contextDetails = agent.getContext(contextName) || { name: contextName, lifespan: 1, parameters: {} };
    return contextDetails;
}
module.exports.setContext = (agent, contextName, lifespan, parameters) => {
    agent.setContext({ name: contextName, lifespan: lifespan, parameters: parameters });
}

module.exports.appendContextParams = (agent, contextName, newParameters) => {
    const contextDetails =  this.getContext(agent, contextName);
    const newParams = { ...contextDetails.parameters, ...newParameters };
    console.log('New Params ==> ', newParams);
    agent.setContext({ name: contextName, lifespan: contextDetails.lifespan - 1, parameters: newParams });
}

    