
const {  SessionsClient } = require('dialogflow');
const projectId = 'throq-ofsljm' || process.env.PROJECT_ID; 
const  serviceAccount  = require('./config/gcm-key.json');
const structjson  = require('./structjson');
module.exports.dialogflowGateway = async (request, response, next) => {
    const { queryInput, sessionId } = request.body;     
    const sessionClient = new SessionsClient({ credentials: serviceAccount });        
    const session = sessionClient.sessionPath(projectId, sessionId);               
    const customParam = {
        auth: 'SathishKrishnan'
    }
    const parameters = structjson.jsonToStructProto({
        source: '234567sdfghjkrtyu',
    });
    const responses = await sessionClient.detectIntent({ 
            session, 
            queryInput,  
            queryParams: {
                payload: parameters
            }
     });      
    
    const result = responses[0].queryResult;      
    
    response.send(result);    
};
