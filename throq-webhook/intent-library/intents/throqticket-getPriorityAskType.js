
"use strict";

/**
 * Default Welcome Intent controller
 * @param {object} df webhook fulfillment object
 */
const TICKET_TYPE = [
    'STORY',
    'BUG',
    'TASK',
]
const { WebhookClient } = require('dialogflow-fulfillment');
const { Card, Suggestion } = require('dialogflow-fulfillment');
const { getContext, appendContextParams } = require('../../helper/dfutils');
const intent = async (agent) => {
    appendContextParams(agent,'ticketdetails', agent.parameters );
    agent.add("Can you say the Ticket Type");
    TICKET_TYPE.forEach(ele => agent.add(new Suggestion(ele)))
};

module.exports = intent;
