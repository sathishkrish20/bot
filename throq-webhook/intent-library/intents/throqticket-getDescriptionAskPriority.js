
"use strict";

/**
 * Default Welcome Intent controller
 * @param {object} df webhook fulfillment object
 */
const PRIORITES = [
    'HIGH',
    'LOW',
    'MEDIUM',
]
const { Card, Suggestion } = require('dialogflow-fulfillment');
const { getContext, appendContextParams } = require('../../helper/dfutils');
const intent = async (agent) => {
    const ticketContextDetails = getContext(agent, 'ticketdetails');
    console.log( 'ticketContextDetails', ticketContextDetails);
    appendContextParams(agent,'ticketdetails', agent.parameters );
    agent.add("Can you Say Priority");
    PRIORITES.forEach(ele => agent.add(new Suggestion(ele)))
};

module.exports = intent;
