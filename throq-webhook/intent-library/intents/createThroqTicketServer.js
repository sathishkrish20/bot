
"use strict";

/**
 * Default Welcome Intent controller
 * @param {object} df webhook fulfillment object
 */
const config = require('../../config/index')();
const { WebhookClient } = require('dialogflow-fulfillment');
const { Card, Suggestion } = require('dialogflow-fulfillment');
const { getContext, appendContextParams } = require('../../helper/dfutils');
const intent = async (agent) => {
    const ticketContextDetails = getContext(agent, 'ticketdetails');
    console.log('ticketContextDetails', ticketContextDetails);
    agent.add("Ticket Has been Created");
};

module.exports = intent;
