
"use strict";

/**
 * Default Welcome Intent controller
 * @param {object} df webhook fulfillment object
 */

const { setContext } = require('../../helper/dfutils');
const PROJECTS = [
    'MEDW',
    'TQR',
    'MEDAP',
]
const intent = async (agent) => {
    console.log(agent.parameters)
    setContext(agent, 'ticketdetails', 12, agent.parameters );
    agent.add(`Alright, Please Enter your Ticket Tittle for ${agent.parameters.projectname}`);
    PROJECTS.forEach(ele => agent.add(new Suggestion(ele)))
};

module.exports = intent;
