
"use strict";

/**
 * Default Welcome Intent controller
 * @param {object} df webhook fulfillment object
 */

const { appendContextParams} = require('../../helper/dfutils');
const intent = async (agent) => {
    console.log(agent.parameters)
    appendContextParams(agent,'ticketdetails', agent.parameters );
    agent.add(`Can you Say Description for ${agent.parameters.ticketTitle}`);
};

module.exports = intent;
