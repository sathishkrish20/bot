
"use strict";

/**
 * Default Welcome Intent controller
 * @param {object} df webhook fulfillment object
 */
const SERVICES = [
    'Create the Ticket',
    'Update the Ticket',
    'Update the Log',
];
const { WebhookClient } = require('dialogflow-fulfillment');
const { Card, Suggestion } = require('dialogflow-fulfillment');
const { getContext, appendContextParams } = require('../../helper/dfutils');
const intent = async (agent) => {
    console.log('HI... Welcome to Virtual Assistant'); 
    agent.add("Hi, Welcome to Throq. I am your Virtual Assistant T-BOT, How can i help you?");
    SERVICES.forEach(ele => agent.add(new Suggestion(ele)))
};

module.exports = intent;
