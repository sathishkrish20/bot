
"use strict";

/**
 * Maps dialogflow intent with its controller
 */

    let intentMap = new Map();
  
intentMap.set('Default Welcome Intent', require("./intents/default-welcome-intent"));

intentMap.set('throqticket-create-getproj-asktitle', require('./intents/throqticket-create-getproj-asktitle') )
intentMap.set('throqticket-getTitleAskDescription', require('./intents/throqticket-getTitleAskDescription'));
intentMap.set('throqticket-getDescriptionAskPriority', require("./intents/throqticket-getDescriptionAskPriority"));
intentMap.set('throqticket-getPriorityAskType', require('./intents/throqticket-getPriorityAskType'));
intentMap.set('createThroqTicketServer', require('./intents/createThroqTicketServer'));
intentMap.set('Default Welcome Intent', require('./intents/default-welcome-intent') );





module.exports = intentMap;
